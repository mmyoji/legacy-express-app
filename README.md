# Legacy Express.js app

Legacy Node.js application uses like these libraries:

- [Express](https://expressjs.com/)
- [Sequelize](https://sequelize.org/)

## Dependencies

- Node.js
- Docker

## Set up

```bash
cp .env.example .env
pnpm install
docker compose up -d
```

## Development

```bash
# Start dev server
pnpm run dev
```

## Production

```bash
export NODE_ENV=production

pnpm install --ignore-scripts
pnpm run build
pnpm run start
```
