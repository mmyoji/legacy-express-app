FROM node:20-slim

WORKDIR /usr/src/app

RUN apt-get update -qq && \
  apt-get install -qq -y --no-install-recommends \
  build-essential \
  default-mysql-client && \
  rm -rf /var/lib/apt/lists/*

RUN corepack enable && \
  corepack prepare pnpm@latest --activate

COPY package.json pnpm-lock.yaml ./

RUN pnpm install --frozen-lockfile

COPY . .
