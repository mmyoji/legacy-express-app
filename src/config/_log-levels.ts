// see: https://getpino.io/#/docs/api?id=level-string
const logLevels = {
  fatal: "fatal",
  error: "error",
  warn: "warn",
  info: "info",
  debug: "debug",
  trace: "trace",
  silent: "silent",
};

export function parseLogLevel(str: string | undefined): string {
  if (typeof str === "string" && Object.values(logLevels).includes(str)) {
    return str;
  }

  return logLevels.info;
}
