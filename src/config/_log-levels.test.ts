import assert from "node:assert/strict";
import { describe, it } from "node:test";

import { parseLogLevel } from "@/config/_log-levels.js";

const defaultLevel = "info";

describe("parseLogLevel()", () => {
  for (const level of [
    "fatal",
    "error",
    "warn",
    "info",
    "debug",
    "trace",
    "silent",
  ]) {
    it(`returns appropriate value with level=${level}`, () => {
      assert.equal(parseLogLevel(level), level);
    });
  }

  for (const level of ["fAtal", "ERROR", "war", ""]) {
    it(`returns default value with invalid value=${level}`, () => {
      assert.equal(parseLogLevel(level), defaultLevel);
    });
  }

  it(`returns default value with undefined`, () => {
    assert.equal(parseLogLevel(undefined), defaultLevel);
  });
});
