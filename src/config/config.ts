import assert from "node:assert/strict";
import { env } from "node:process";

import { parseLogLevel } from "@/config/_log-levels.js";

assert(env.JWT_PRIVATE_KEY);

export const config = {
  app: {
    name: env.APP_NAME ?? "app",
  },
  db: {
    username: env.DB_USERNAME ?? "root",
    password: env.DB_PASSWORD ?? "password",
    database: env.DB_DATABASE ?? "app_development",
    host: env.DB_HOST ?? "127.0.0.1",
    port: Number(env.DB_PORT) || 3306,
    dialect: "mysql" as const,
    define: {
      charset: "utf8",
      dialectOptions: {
        collate: "utf8_general_ci",
      },
      underscored: true,
    },
    logging: !!env.DB_LOGGING,
  },
  jwt: {
    prvkey: env.JWT_PRIVATE_KEY,
  },
  server: {
    port: Number(env.PORT) || 3000,
    logLevel: parseLogLevel(env.LOG_LEVEL),
  },
} as const;
