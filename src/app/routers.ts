import type { AppRouter } from "@/lib/router.utils.js";

import { rootRouter } from "@/app/root/root.router.js";
import { tokensRouter } from "@/app/tokens/tokens.router.js";
import { usersRouter } from "@/app/users/users.router.js";

export const routers: AppRouter[] = [rootRouter, tokensRouter, usersRouter];
