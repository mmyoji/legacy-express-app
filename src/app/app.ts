import express from "express";
import helmet from "helmet";

import { routers } from "@/app/routers.js";
import { initI18n } from "@/lib/i18n.js";
import { authErrorHandler } from "@/lib/middlewares/auth-error.handler.js";
import { i18n } from "@/lib/middlewares/i18n.js";
import { logger } from "@/lib/middlewares/logger.js";
import { notFoundHandler } from "@/lib/middlewares/not-found.handler.js";
import { serverErrorHandler } from "@/lib/middlewares/server-error.handler.js";
import { validationErrorHandler } from "@/lib/middlewares/validation-error.handler.js";

const app = express();

export type App = express.Application;

export async function initApp(): Promise<App> {
  const i18next = await initI18n();

  app.disable("x-powered-by");

  app.use(helmet());
  app.use(express.json());
  app.use(logger());
  app.use(i18n(i18next));

  for (const { base, init } of routers) {
    app.use(base, init({ strict: true }));
  }

  app.use(notFoundHandler());
  app.use(authErrorHandler());
  app.use(validationErrorHandler());
  app.use(serverErrorHandler());

  return app;
}
