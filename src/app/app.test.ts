import assert from "node:assert/strict";
import { before, describe, it } from "node:test";

import request from "supertest";

import { type App, initApp } from "@/app/app.js";

describe("404", () => {
  let app: App;

  before(async () => {
    app = await initApp();
  });

  it("returns 404 for unknown path", async () => {
    const res = await request(app).get("/foo");

    assert.equal(res.status, 404);
    assert.equal(
      res.headers["content-type"],
      "application/json; charset=utf-8",
    );
    assert.deepEqual(res.body, { errors: [`/foo: Page not found`] });
  });
});
