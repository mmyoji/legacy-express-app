import { indexHandler } from "@/app/users/users.handlers.js";
import { defineRouter } from "@/lib/router.utils.js";

export const usersRouter = defineRouter("/users", (r) => {
  r.get("/", indexHandler);
});
