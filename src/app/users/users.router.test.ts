import assert from "node:assert/strict";
import { before, beforeEach, describe, it } from "node:test";

import request from "supertest";

import { type App, initApp } from "@/app/app.js";

import { factory, sleep, useDatabase } from "@/testutils/mod.js";

describe("GET /users", () => {
  useDatabase();

  let app: App;
  let id1: number;
  let id2: number;
  let id3: number;

  before(async () => {
    app = await initApp();
  });

  beforeEach(async () => {
    id1 = (await factory.user.create({ username: "buz" })).id;
    await sleep(1000);
    id2 = (await factory.user.create({ username: "bar" })).id;
    await sleep(1000);
    id3 = (await factory.user.create({ username: "foo" })).id;
  });

  it("returns 200", async () => {
    const res = await request(app).get("/users");

    assert.equal(res.status, 200);
    assert.deepEqual(res.body, {
      data: {
        users: [
          { id: id3, username: "foo" },
          { id: id2, username: "bar" },
          { id: id1, username: "buz" },
        ],
      },
    });
  });
});
