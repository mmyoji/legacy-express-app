import { User } from "@/db/models/mod.js";
import { defineHandler } from "@/lib/router.utils.js";

export const indexHandler = defineHandler(async (_, res) => {
  const users = await User.findAll({
    attributes: ["id", "username"],
    order: [["created_at", "DESC"]],
  });
  res.json({ data: { users } });
});
