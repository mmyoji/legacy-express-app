import { healthHandler, indexHandler } from "@/app/root/root.handlers.js";
import { defineRouter } from "@/lib/router.utils.js";

export const rootRouter = defineRouter("/", (r) => {
  r.get("/", indexHandler);

  r.get("/_health", healthHandler);
});
