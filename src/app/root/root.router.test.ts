import assert from "node:assert/strict";
import { before, describe, it } from "node:test";

import request from "supertest";

import { type App, initApp } from "@/app/app.js";

describe("GET /", () => {
  let app: App;

  before(async () => {
    app = await initApp();
  });

  it("returns 200", async () => {
    const res = await request(app).get("/");

    assert.equal(res.status, 200);
    assert.deepEqual(res.body, { message: "Hi!" });
  });

  it("returns 200 w/ jp message when lng=ja", async () => {
    const res = await request(app).get("/?lng=ja");

    assert.equal(res.status, 200);
    assert.deepEqual(res.body, { message: "どうも！" });
  });

  it("returns 200 w/ jp message when Accept-Language=ja", async () => {
    const res = await request(app).get("/").set("Accept-Language", "ja");

    assert.equal(res.status, 200);
    assert.deepEqual(res.body, { message: "どうも！" });
  });
});

describe("GET /_health", () => {
  let app: App;

  before(async () => {
    app = await initApp();
  });

  it("returns 200", async () => {
    const res = await request(app).get("/_health");

    assert.equal(res.status, 200);
    assert.deepEqual(res.body, { message: "OK" });
  });
});
