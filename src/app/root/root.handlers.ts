import { defineHandler } from "@/lib/router.utils.js";

export const indexHandler = defineHandler((req, res) => {
  res.json({ message: req.t("hello") });
});

export const healthHandler = defineHandler((_, res) => {
  res.json({ message: "OK" });
});
