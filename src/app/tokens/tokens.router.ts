import { checkHandler, createHandler } from "@/app/tokens/tokens.handlers.js";
import { auth } from "@/lib/middlewares/auth.js";
import { defineRouter } from "@/lib/router.utils.js";

export const tokensRouter = defineRouter("/tokens", (r) => {
  r.post("/", createHandler);

  r.get("/_check", auth(), checkHandler);
});
