import assert from "node:assert";

import { beforeEach, describe, it } from "node:test";
import { ZodError } from "zod";

import { validate } from "@/app/tokens/tokens.validator.js";
import { helpers } from "@/testutils/mod.js";

describe("validate()", () => {
  beforeEach(async () => {
    await helpers.initZodI18n();
  });

  it("returns an object with valid arg", async () => {
    const obj = await validate({ username: "john", password: "P@s$w0rd" });

    assert.equal(obj.username, "john");
    assert.equal(obj.password, "P@s$w0rd");
  });

  it("raises ZodError without username", async () => {
    try {
      await validate({ password: "P@s$w0rd" });
      assert.fail();
    } catch (err) {
      assert(err instanceof ZodError);
      assert.equal(err.issues.length, 1);

      const [issue] = err.issues;
      assert.equal(issue.path[0], "username");
      assert.equal(issue.message, "Username is required");
    }
  });

  it("raises ZodError without password", async () => {
    try {
      await validate({ username: "john" });
      assert.fail();
    } catch (err) {
      assert(err instanceof ZodError);
      assert.equal(err.issues.length, 1);

      const [issue] = err.issues;
      assert.equal(issue.path[0], "password");
      assert.equal(issue.message, "Password is required");
    }
  });
});
