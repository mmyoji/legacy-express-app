import assert from "node:assert/strict";
import { before, beforeEach, describe, it } from "node:test";

import request from "supertest";

import { type App, initApp } from "@/app/app.js";
import { factory, helpers, useDatabase } from "@/testutils/mod.js";

describe("POST /tokens", () => {
  useDatabase();

  let app: App;

  const username = "foo";
  const password = "P@s$w0rd";

  before(async () => {
    app = await initApp();
  });

  beforeEach(async () => {
    const user = await factory.user.create({ username });
    await factory.login.create({ password, userId: user.id });
  });

  const subject = (json: object) =>
    request(app)
      .post("/tokens")
      .set("Content-Type", "application/json")
      .send(json);

  it("returns 201 with valid credentials", async () => {
    const res = await subject({ username, password });

    assert.equal(res.status, 201);
    assert.equal(typeof res.body.data.access_token, "string");
  });

  it("returns 422 with invalid username", async () => {
    const res = await subject({ username: "unknown-user", password });

    assert.equal(res.status, 422);
    assert.deepEqual(res.body, {
      errors: ["username or password is invalid"],
    });
  });

  it("returns 422 with short password", async () => {
    const res = await subject({ username, password: "pass" });

    assert.equal(res.status, 422);
    assert.deepEqual(res.body, {
      errors: ["Password must contain at least 8 character(s)"],
    });
  });

  it("returns 422 with invalid password", async () => {
    const res = await subject({ username, password: "password" });

    assert.equal(res.status, 422);
    assert.deepEqual(res.body, {
      errors: ["username or password is invalid"],
    });
  });
});

describe("GET /tokens/_check", () => {
  let app: App;

  before(async () => {
    app = await initApp();
  });

  it("returns 200 with valid token", async () => {
    const res = await request(app)
      .get("/tokens/_check")
      .set(...helpers.authHeaders("john"));

    assert.equal(res.status, 200);
    assert.deepEqual(res.body, { message: "OK" });
  });

  it("returns 401 with invalid credentials", async () => {
    const res = await request(app).get("/tokens/_check");

    assert.equal(res.status, 401);
    assert.deepEqual(res.body, { errors: ["Authorization required"] });
  });
});
