import { validate } from "@/app/tokens/tokens.validator.js";
import { Login, User } from "@/db/models/mod.js";
import { ValidationError } from "@/lib/errors/validation.error.js";
import { generateToken } from "@/lib/jwt.js";
import { defineHandler } from "@/lib/router.utils.js";

export const createHandler = defineHandler(async (req, res) => {
  const { t } = req;
  const { username, password } = await validate(req.body);

  const login = await Login.findOne({
    include: { model: User, where: { username } },
  });
  if (!login) {
    throw new ValidationError(t(`errors.invalid_credentials`));
  }

  const result = await login.verify(password);
  if (!result) {
    throw new ValidationError(t(`errors.invalid_credentials`));
  }

  const access_token = generateToken(username);

  res.status(201).json({ data: { access_token } });
});

export const checkHandler = defineHandler((_, res) => {
  res.status(200).json({ message: "OK" });
});
