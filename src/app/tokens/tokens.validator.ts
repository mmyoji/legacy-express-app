import { z } from "zod";

const schema = z.object({
  username: z
    .string()
    .min(1)
    .max(20)
    .regex(/[a-zA-Z0-9]+/),
  password: z
    .string()
    .min(8)
    .max(72)
    .regex(/[a-zA-Z0-9@#$%&*]+/),
});

type Schema = z.infer<typeof schema>;

export function validate(body: unknown): Promise<Schema> {
  return schema.parseAsync(body);
}
