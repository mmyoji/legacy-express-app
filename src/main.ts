import { Server } from "node:http";

import { initApp } from "@/app/app.js";
import { config } from "@/config/config.js";
import { sequelize } from "@/db/models/mod.js";
import { logger } from "@/lib/logger.js";

const { port } = config.server;

let server: Server | undefined;

(async () => {
  await sequelize.authenticate();
  const app = await initApp();
  server = app.listen(port, () => {
    logger.info(`Legacy express app listening on port ${port}`);
  });
})().catch((err) => {
  logger.error(err);
});

const shutdown = () => {
  if (server == null) return;

  let code: 0 | 1 = 0;

  server.close((err) => {
    (async () => {
      await sequelize.close();
    })()
      .catch((e) => {
        if (e) {
          logger.error(e);
          code = 1;
        }
      })
      .finally(() => {
        if (err) {
          logger.error(err);
          code = 1;
        }

        process.exit(code);
      });
  });
};

process.on("SIGINT", shutdown);
process.on("SIGTERM", shutdown);
