import { generateToken } from "@/lib/jwt.js";

export function authHeaders(username: string): [string, string] {
  const token = generateToken(username);
  return ["Authorization", `Bearer ${token}`];
}
