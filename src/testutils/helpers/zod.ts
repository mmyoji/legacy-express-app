import i18next from "i18next";
import { z } from "zod";
import { makeZodI18nMap } from "zod-i18n-map";
import zod from "@/config/locales/en/zod.json";

export async function initZodI18n() {
  await i18next.init({
    lng: "en",
    resources: {
      en: {
        zod,
      },
    },
  });

  z.setErrorMap(
    makeZodI18nMap({
      t: i18next.t,
    }),
  );
}
