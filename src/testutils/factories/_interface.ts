export type Factory<Props, Model> = {
  build(props: Props): Model;
  create(props: Props): Promise<Model>;
};
