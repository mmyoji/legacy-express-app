import type { Factory } from "@/testutils/factories/_interface.js";

import { User } from "@/db/models/user.js";

type Props = {
  username: string;
};

export const user: Factory<Props, User> = {
  build(props: Props) {
    return User.build(props);
  },

  create(props: Props) {
    return User.create(props);
  },
};
