import type { Factory } from "@/testutils/factories/_interface.js";

import { Login } from "@/db/models/login.js";
import { hashPasswordSync } from "@/lib/password.js";

type Props = {
  userId: number;
  password?: string;
};

export const login: Factory<Props, Login> = {
  build({ password, ...rest }: Props) {
    return Login.build({
      ...rest,
      encryptedPassword: hashPasswordSync(password ?? "P@s$w0rd"),
    });
  },

  async create(props: Props) {
    const model = this.build(props);
    await model.save();
    return model;
  },
};
