import { login } from "@/testutils/factories/login.js";
import { user } from "@/testutils/factories/user.js";

export const factory = {
  login,
  user,
};
