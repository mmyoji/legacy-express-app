import { after, afterEach, before, beforeEach } from "node:test";
import { Transaction } from "sequelize";

import { ns, sequelize } from "@/db/models/mod.js";

// ref https://github.com/sequelize/sequelize/issues/11408#issuecomment-1102321495
export function useDatabase() {
  let txAll: Transaction;
  let txEach: Transaction;

  const autocommit = false;

  before(async () => {
    txAll = await sequelize.transaction({ autocommit });

    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
    const ctx = ns.createContext();
    ns.enter(ctx);
    ns.set("transaction", txAll);
  });

  beforeEach(async () => {
    txEach = await sequelize.transaction({
      autocommit,
      transaction: txAll,
    });

    ns.set("transaction", txEach);
  });

  afterEach(async () => {
    await txEach.rollback();
  });

  after(async () => {
    await txAll.rollback();
    await sequelize.close();
  });
}
