import assert from "node:assert/strict";
import { describe, it } from "node:test";

import { hashPassword, verifyPassword } from "@/lib/password.js";

describe("password", () => {
  it("generates hashed password and can verifies", async () => {
    const password = "P@s$w0rd";

    const hashed = await hashPassword(password);

    assert.notEqual(hashed, password);
    assert.equal(typeof hashed, "string");

    let result = await verifyPassword(password, hashed);

    assert.equal(result, true);

    result = await verifyPassword("password", hashed);

    assert.equal(result, false);
  });
});
