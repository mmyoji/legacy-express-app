import {
  Router,
  type NextFunction,
  type Request,
  type RequestHandler,
  type Response,
  type RouterOptions,
} from "express";

export function defineHandler(
  fn: (req: Request, res: Response, next: NextFunction) => Promise<void> | void,
): RequestHandler {
  return (req, res, next) => {
    const ret = fn(req, res, next);

    if (ret != null) {
      ret.catch(next);
    }
  };
}
type RouterInit = (options: RouterOptions) => Router;

export type AppRouter = {
  base: `/${string}`;
  init: RouterInit;
};

export function defineRouter(
  base: AppRouter["base"],
  fn: (router: Router) => void,
): AppRouter {
  return {
    base,
    init: (options) => {
      const router = Router(options);

      fn(router);

      return router;
    },
  };
}
