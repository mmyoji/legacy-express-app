import assert from "node:assert";
import { describe, it } from "node:test";

import { AuthError } from "@/lib/errors/auth.error.js";
import { generateToken, verifyToken } from "@/lib/jwt.js";

describe("generateToken()", () => {
  it("generates JWT", () => {
    const token = generateToken("foo");

    assert.equal(typeof token, "string");
    assert.equal(token.split(".").length, 3);
  });
});

describe("verifyToken()", () => {
  it("throws an error w/ invalid token", async () => {
    const token = "foo";

    try {
      await verifyToken(token);
      assert.fail();
    } catch (err) {
      assert(err instanceof AuthError);
      assert.equal(err.message, "Invalid JWT");
    }
  });

  it("returns an object w/ valid token", async () => {
    const token = generateToken("foo");

    const user = await verifyToken(token);

    assert.equal(user.username, "foo");
  });
});
