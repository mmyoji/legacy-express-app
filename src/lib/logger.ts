import { pino } from "pino";

import { config } from "@/config/config.js";

export const logger = pino({
  name: config.app.name,
  level: config.server.logLevel,
});
