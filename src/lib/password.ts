import { compare, hash, hashSync } from "bcrypt";

const SALT_ROUNDS = 10;

export function hashPassword(password: string): Promise<string> {
  return hash(password, SALT_ROUNDS);
}

/**
 * Only for test
 */
export function hashPasswordSync(password: string): string {
  return hashSync(password, SALT_ROUNDS);
}

export function verifyPassword(
  password: string,
  encryptedPassword: string,
): Promise<boolean> {
  return compare(password, encryptedPassword);
}
