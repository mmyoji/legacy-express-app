import { dirname as _dirname } from "node:path";
import { fileURLToPath } from "node:url";

export function filename(metaUrl: string): string {
  return fileURLToPath(metaUrl);
}

export function dirname(metaUrl: string): string {
  return _dirname(filename(metaUrl));
}
