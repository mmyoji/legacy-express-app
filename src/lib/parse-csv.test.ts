import assert from "node:assert/strict";
import { join } from "node:path";
import { describe, it } from "node:test";

import { parseCSV } from "@/lib/parse-csv.js";
import { dirname } from "@/lib/path.utils.js";

const __dirname = dirname(import.meta.url);

describe("parseCSV()", () => {
  it("returns an array of objects", async () => {
    const path = join(__dirname, "../testutils/data/users.csv");

    const records = await parseCSV<{ id: string; name: string; age: string }>(
      path,
    );

    assert.deepEqual(records, [
      { id: "1", name: "foo", age: "10" },
      { id: "2", name: "bar", age: "20" },
    ]);
  });

  it("returns an array of objects with custom keys", async () => {
    const path = join(__dirname, "../testutils/data/users-without-headers.csv");

    const records = await parseCSV<{
      userId: string;
      username: string;
      age: string;
    }>(path, ["userId", "username", "age"]);

    assert.deepEqual(records, [
      { userId: "1", username: "foo", age: "10" },
      { userId: "2", username: "bar", age: "20" },
    ]);
  });
});
