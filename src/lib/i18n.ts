import { join } from "node:path";

import i18next from "i18next";
import fsBackend, { type FsBackendOptions } from "i18next-fs-backend";
import { LanguageDetector, type I18next } from "i18next-http-middleware";
import { z } from "zod";
import { makeZodI18nMap } from "zod-i18n-map";

import { dirname } from "@/lib/path.utils.js";

export async function initI18n(): Promise<I18next> {
  await i18next
    .use(fsBackend)
    .use(LanguageDetector)
    .init<FsBackendOptions>({
      fallbackLng: "en",
      supportedLngs: ["en", "ja"],
      preload: ["en", "ja"],
      ns: ["translation", "zod"],
      backend: {
        loadPath: join(
          dirname(import.meta.url),
          "../config/locales/{{lng}}/{{ns}}.json",
        ),
      },
    });

  z.setErrorMap(makeZodI18nMap({ t: i18next.t }));

  return i18next;
}
