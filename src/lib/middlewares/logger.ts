import { pinoHttp } from "pino-http";

import { logger as _logger } from "@/lib/logger.js";

export const logger = () => pinoHttp({ logger: _logger });
