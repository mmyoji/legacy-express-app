import { handle, type I18next } from "i18next-http-middleware";

export function i18n(mod: I18next) {
  return handle(mod);
}
