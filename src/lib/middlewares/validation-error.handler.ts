import type { ErrorRequestHandler } from "express";
import { ZodError } from "zod";

import { ValidationError } from "@/lib/errors/validation.error.js";

export function validationErrorHandler(): ErrorRequestHandler {
  return (err, _req, res, next) => {
    if (err instanceof ZodError) {
      const errors = err.issues.map(({ message }) => message);
      res.status(422).json({ errors });
      return;
    }

    if (err instanceof ValidationError) {
      res.status(422).json({ errors: [err.message] });
      return;
    }

    next(err);
  };
}
