import type { RequestHandler } from "express";

import { AuthError } from "@/lib/errors/auth.error.js";
import { verifyToken } from "@/lib/jwt.js";

export function auth(): RequestHandler {
  return (req, res, next) => {
    // "Bearer xxx"
    const { authorization } = req.headers;

    if (!authorization) {
      return next(new AuthError(`Authorization required`));
    }

    const [_, token] = authorization.split("Bearer ");
    verifyToken(token)
      .then((user) => {
        res.locals.user = user;
        next();
      })
      .catch((err) => {
        next(err);
      });
  };
}
