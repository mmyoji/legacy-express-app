import type { ErrorRequestHandler } from "express";

import { AuthError } from "@/lib/errors/auth.error.js";

export function authErrorHandler(): ErrorRequestHandler {
  return (err, _req, res, next) => {
    if (!(err instanceof AuthError)) {
      return next(err);
    }

    const { message } = err;
    const errors = [message];
    res.status(401).json({ errors });
  };
}
