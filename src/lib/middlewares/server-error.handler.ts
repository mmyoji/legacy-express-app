import type { ErrorRequestHandler } from "express";

export function serverErrorHandler(): ErrorRequestHandler {
  return (err, req, res) => {
    req.log.error(err instanceof Error ? err.message : JSON.stringify(err));
    res.status(500).json({ messages: [`Something went wrong!`] });
  };
}
