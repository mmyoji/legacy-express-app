import jwt from "jsonwebtoken";

import { config } from "@/config/config.js";
import { AuthError } from "@/lib/errors/auth.error.js";

const prvkey = config.jwt.prvkey;
const expiresIn = "2 weeks";

export function generateToken(username: string): string {
  return jwt.sign({ username }, prvkey, {
    expiresIn,
  });
}

export async function verifyToken(
  token: string,
): Promise<{ username: string }> {
  return new Promise((resolve, reject) => {
    jwt.verify(token, prvkey, (err, decoded) => {
      if (err) {
        return reject(new AuthError(`Invalid JWT`));
      }

      const user = {
        username: (decoded as { username: string }).username,
      };

      resolve(user);
    });
  });
}
