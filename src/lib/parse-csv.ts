import { readFile, stat } from "node:fs/promises";
import { promisify } from "node:util";

import { parse, type Options } from "csv-parse";

type Row = Record<string, string>;

const parseAsync: <T>(
  input: Buffer | string,
  options: Options,
) => Promise<T[]> = promisify(parse);

export async function parseCSV<T extends Row>(
  path: string,
  columns?: string[],
): Promise<T[]> {
  await stat(path);
  const content = await readFile(path);
  return await parseAsync<T>(content, { columns: columns ?? true });
}
