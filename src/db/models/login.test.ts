import assert from "node:assert/strict";
import { beforeEach, describe, it } from "node:test";

import { factory, useDatabase } from "@/testutils/mod.js";

describe("Login", () => {
  useDatabase();

  let userId: number;

  beforeEach(async () => {
    const user = await factory.user.create({
      username: "test-user1",
    });
    userId = user.id;
  });

  describe("#verify", () => {
    it("returns true when given password is valid", async () => {
      const login = factory.login.build({
        userId,
        password: "passwd",
      });

      assert.equal(await login.verify("passwd"), true);

      assert.equal(await login.verify("password"), false);
    });
  });
});
