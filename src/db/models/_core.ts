import { Sequelize } from "sequelize";
import { createNamespace } from "cls-hooked";

import { config } from "@/config/config.js";

export const ns = createNamespace("myapp");

Sequelize.useCLS(ns);

export const sequelize = new Sequelize(config.db);
