import {
  Association,
  CreationOptional,
  DataTypes,
  InferAttributes,
  InferCreationAttributes,
  Model,
  NonAttribute,
} from "sequelize";

import { sequelize } from "@/db/models/_core.js";
import { Login } from "@/db/models/login.js";

class User extends Model<
  InferAttributes<User, { omit: "login" }>,
  InferCreationAttributes<User, { omit: "login" }>
> {
  declare id: CreationOptional<number>;
  declare username: string;
  declare createdAt: CreationOptional<Date>;
  declare updatedAt: CreationOptional<Date>;

  declare login?: NonAttribute<Login>;

  declare static associations: {
    login: Association<User, Login>;
  };
}

User.init(
  {
    id: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
    },
    username: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
    createdAt: {
      type: "DATETIME DEFAULT CURRENT_TIMESTAMP",
      allowNull: false,
    },
    updatedAt: {
      type: "DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP",
      allowNull: false,
    },
  },
  {
    sequelize,
    tableName: "users",
  },
);

export { User };
