import {
  Association,
  CreationOptional,
  DataTypes,
  InferAttributes,
  InferCreationAttributes,
  Model,
  NonAttribute,
} from "sequelize";

import { sequelize } from "@/db/models/_core.js";
import { User } from "@/db/models/user.js";
import { verifyPassword } from "@/lib/password.js";

class Login extends Model<
  InferAttributes<Login, { omit: "user" }>,
  InferCreationAttributes<Login, { omit: "user" }>
> {
  declare id: CreationOptional<number>;
  declare userId: number;
  declare encryptedPassword: string;
  declare createdAt: CreationOptional<Date>;
  declare updatedAt: CreationOptional<Date>;

  declare user?: NonAttribute<User>;

  declare static associations: {
    user: Association<Login, User>;
  };

  verify(password: string): Promise<boolean> {
    return verifyPassword(password, this.encryptedPassword);
  }
}

Login.init(
  {
    id: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
    },
    userId: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      unique: true,
      references: {
        model: {
          tableName: "users",
        },
        key: "id",
      },
      onDelete: "CASCADE",
    },
    encryptedPassword: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    createdAt: {
      type: "DATETIME DEFAULT CURRENT_TIMESTAMP",
      allowNull: false,
    },
    updatedAt: {
      type: "DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP",
      allowNull: false,
    },
  },
  {
    sequelize,
    tableName: "logins",
  },
);

export { Login };
