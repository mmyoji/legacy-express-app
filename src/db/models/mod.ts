export { ns, sequelize } from "@/db/models/_core.js";

import { Login } from "@/db/models/login.js";
import { User } from "@/db/models/user.js";

User.hasOne(Login, {
  onDelete: "CASCADE",
});
Login.belongsTo(User);

export { Login, User };
