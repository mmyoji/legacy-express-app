const { DataTypes, literal } = require("sequelize");

async function up(queryInterface) {
  await queryInterface.createTable(
    "logins",
    {
      id: {
        type: DataTypes.BIGINT.UNSIGNED,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      userId: {
        type: DataTypes.BIGINT.UNSIGNED,
        allowNull: false,
        unique: true,
        field: "user_id",
        references: {
          model: {
            tableName: "users",
          },
          key: "id",
        },
        onDelete: "CASCADE",
      },
      encryptedPassword: {
        type: DataTypes.STRING,
        allowNull: false,
        field: "encrypted_password",
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: literal("CURRENT_TIMESTAMP"),
        field: "created_at",
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: literal("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"),
        field: "updated_at",
      },
    },
    {
      charset: "utf8",
      collate: "utf8_general_ci",
    },
  );
}

async function down(queryInterface) {
  await queryInterface.dropTable("logins");
}

module.exports = {
  up,
  down,
};
