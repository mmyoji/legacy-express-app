const { DataTypes, literal } = require("sequelize");

async function up(queryInterface) {
  await queryInterface.createTable(
    "users",
    {
      id: {
        type: DataTypes.BIGINT.UNSIGNED,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      username: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: literal("CURRENT_TIMESTAMP"),
        field: "created_at",
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: literal("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"),
        field: "updated_at",
      },
    },
    {
      charset: "utf8",
      collate: "utf8_general_ci",
    },
  );
}

async function down(queryInterface) {
  await queryInterface.dropTable("users");
}

module.exports = {
  up,
  down,
};
