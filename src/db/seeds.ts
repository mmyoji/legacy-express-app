// We can't use seed feature of sequelize-cli
// because it doesn't support ESM.

import { sequelize, Login, User } from "@/db/models/mod.js";
import { hashPassword } from "@/lib/password.js";

async function main() {
  const usernames = ["foo", "bar", "buz"];

  for (const username of usernames) {
    const transaction = await sequelize.transaction();
    try {
      const [user] = await User.findOrCreate({
        where: {
          username,
        },
        transaction,
      });
      await Login.findOrCreate({
        where: {
          userId: user.id,
        },
        // @ts-expect-error it's okay
        defaults: {
          encryptedPassword: await hashPassword("P@s$w0rd"),
        },
        transaction,
      });

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
    }
  }
}

main().catch(console.error);
