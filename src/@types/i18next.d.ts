// import the original type declarations
import "i18next";

import translation from "@/config/locales/en/translation.json";

declare module "i18next" {
  /* eslint-disable-next-line */
  interface CustomTypeOptions {
    resources: {
      translation: typeof translation;
    };
  }
}
