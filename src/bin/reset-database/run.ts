import { run } from "@/bin/_utils/run.js";
import { sequelize } from "@/db/models/mod.js";

run(async function main() {
  await sequelize.sync({ force: true });
});
