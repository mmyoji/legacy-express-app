import { exit } from "node:process";

export function run(fn: () => Promise<void>) {
  let exitCode = 0;

  fn()
    .catch((err) => {
      exitCode = 1;
      console.error(err);
    })
    .finally(() => {
      exit(exitCode);
    });
}
