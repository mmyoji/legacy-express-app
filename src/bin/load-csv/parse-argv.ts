import { parseArgs as _parseArgs } from "node:util";

type Options = {
  dryRun: boolean;
  path: string;
};

const HELP = `
Usage: node dist/bin/load-csv/run.js --path=/tmp/20230224_users.csv

  Required Options:

    -p, --path <CSV path>  file path to load

  Options:

    --dry-run          Dry run
    -h, --help         Show help message
`;

export function parseArgv(args: string[]): Options | string {
  const { values } = _parseArgs({
    args,
    options: {
      path: {
        type: "string",
        short: "p",
        default: "",
      },
      "dry-run": {
        type: "boolean",
        default: false,
      },
      help: {
        type: "boolean",
        short: "h",
        default: false,
      },
    },
  });

  if (values.help) {
    return HELP;
  }

  const options: Options = {
    path: values.path ?? "",
    dryRun: values["dry-run"] ?? false,
  };

  if (!options.path) {
    throw new Error(`--path=<CSV path> must be passed.`);
  }

  return options;
}
