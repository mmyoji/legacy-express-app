import { argv } from "node:process";

import { run } from "@/bin/_utils/run.js";
import { parseArgv } from "@/bin/load-csv/parse-argv.js";
import { parseCSV } from "@/lib/parse-csv.js";

type Row = {
  id: string;
  name: string;
  age: string;
};

run(async function main() {
  const options = parseArgv(argv.slice(2));
  if (typeof options === "string") {
    console.info(options);
    return;
  }

  const { dryRun, path } = options;
  if (dryRun) {
    console.info("=== DRY RUN starts ===");
  }

  const records = await parseCSV<Row>(path);

  for (const row of records) {
    if (dryRun) {
      console.info({ row });
      continue;
    }

    // TODO: INSERT INTO users;
  }

  if (dryRun) {
    console.info("=== DRY RUN ends ===");
  }
});
