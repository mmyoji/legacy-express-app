import assert from "node:assert";
import { describe, it } from "node:test";

import { parseArgv } from "@/bin/load-csv/parse-argv.js";

describe("parseArgv()", () => {
  const csvPath = "/tmp/test.csv";

  for (const { argv } of [
    { argv: ["-h"] },
    { argv: ["--help"] },
    { argv: ["-h", "--path", csvPath] },
    { argv: ["--path", csvPath, "--help"] },
  ]) {
    it(`returns help message when -h or --help is passed (${JSON.stringify(
      argv,
    )})`, () => {
      const ret = parseArgv(argv);

      assert(typeof ret === "string");
      assert(ret.startsWith("\nUsage: node dist/bin/load-csv/run.js"));
    });
  }

  for (const { argv } of [{ argv: ["-p"] }, { argv: ["--path"] }]) {
    it(`throws an error without :path (${JSON.stringify(argv)})`, () => {
      try {
        parseArgv(argv);
        assert.fail();
      } catch (err) {
        assert(err instanceof Error);
        assert.equal(
          err.message,
          "Option '-p, --path <value>' argument missing",
        );
      }
    });
  }

  for (const { argv } of [
    { argv: [] },
    { argv: ["--dry-run"] },
    { argv: ["--path", ""] },
  ]) {
    it(`throws an error without :path (${JSON.stringify(argv)})`, () => {
      try {
        parseArgv(argv);
        assert.fail();
      } catch (err) {
        assert(err instanceof Error);
        assert.equal(err.message, "--path=<CSV path> must be passed.");
      }
    });
  }

  for (const { argv, expected } of [
    {
      argv: ["--dry-run", "-p", csvPath],
      expected: { path: csvPath, dryRun: true },
    },
    {
      argv: ["--path", csvPath, "--dry-run"],
      expected: { path: csvPath, dryRun: true },
    },
    {
      argv: [`--path=${csvPath}`],
      expected: { path: csvPath, dryRun: false },
    },
    { argv: ["-p", csvPath], expected: { path: csvPath, dryRun: false } },
  ]) {
    it(`returns Options object with argv=${JSON.stringify(argv)}`, () => {
      const ret = parseArgv(argv);

      assert.deepEqual(ret, expected);
    });
  }
});
